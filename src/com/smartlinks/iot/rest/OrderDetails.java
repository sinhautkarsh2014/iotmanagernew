package com.smartlinks.iot.rest;

import java.lang.Math;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// Plain old Java Object it does not extend as class or implements
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation.
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML.

// The browser requests per default the HTML MIME type.

//Sets the path to base URL + /hello
@Path("/od")
public class OrderDetails {

  // This method is called if TEXT_PLAIN is request
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String sayPlainTextHello() {
	double[] increments={0.1220000,0.0210000,0.1420000,0.0540000,0.1110000,0.1320000,0.0120000};
	double randlat=increments[(int) Math.floor(Math.random() * increments.length)];
	double randlng=increments[(int) Math.floor(Math.random() * increments.length)];  
	String freight="[{\"coords\":{\"lat\":"+String.format("%.7f", (54.9619349-randlat))+",\"lng\":"+String.format("%.7f", (-1.6003813-randlng))+"},\"truckid\":\"a1\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9656694-randlat))+",\"lng\":"+String.format("%.7f", (-1.5239833-randlng))+"},\"truckid\":\"a2\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9696456-randlat))+",\"lng\":"+String.format("%.7f", (-1.5069755-randlng))+"},\"truckid\":\"a3\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9378907-randlat))+",\"lng\":"+String.format("%.7f", (-1.5273729-randlng))+"},\"truckid\":\"a4\"}]";
    return freight;
  }

  // This method is called if XML is request
  @GET
  @Produces(MediaType.TEXT_XML)
  public String sayXMLHello() {
	double[] increments={0.1220000,0.0210000,0.1420000,0.0540000,0.1110000,0.1320000,0.0120000};
	double randlat=increments[(int) Math.floor(Math.random() * increments.length)];
	double randlng=increments[(int) Math.floor(Math.random() * increments.length)];
	String freight="[{\"coords\":{\"lat\":"+String.format("%.7f", (54.9619349-randlat))+",\"lng\":"+String.format("%.7f", (-1.6003813-randlng))+"},\"truckid\":\"a1\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9656694-randlat))+",\"lng\":"+String.format("%.7f", (-1.5239833-randlng))+"},\"truckid\":\"a2\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9696456-randlat))+",\"lng\":"+String.format("%.7f", (-1.5069755-randlng))+"},\"truckid\":\"a3\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9378907-randlat))+",\"lng\":"+String.format("%.7f", (-1.5273729-randlng))+"},\"truckid\":\"a4\"}]";
	return "<?xml version=\"1.0\"?>" + "<hello>" + freight + "</hello>";
  }

  // This method is called if HTML is request
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String sayHtmlHello() {
	double[] increments={0.1220000,0.0210000,0.1420000,0.0540000,0.1110000,0.1320000,0.0120000};
	double randlat=increments[(int) Math.floor(Math.random() * increments.length)];
	double randlng=increments[(int) Math.floor(Math.random() * increments.length)];  
	String freight="[{\"coords\":{\"lat\":"+String.format("%.7f", (54.9619349-randlat))+",\"lng\":"+String.format("%.7f", (-1.6003813-randlng))+"},\"truckid\":\"a1\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9656694-randlat))+",\"lng\":"+String.format("%.7f", (-1.5239833-randlng))+"},\"truckid\":\"a2\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9696456-randlat))+",\"lng\":"+String.format("%.7f", (-1.5069755-randlng))+"},\"truckid\":\"a3\"},{\"coords\":{\"lat\":"+String.format("%.7f", (54.9378907-randlat))+",\"lng\":"+String.format("%.7f", (-1.5273729-randlng))+"},\"truckid\":\"a4\"}]";
    return "<html> " + "<title>" + freight + "</title>"
        + "<body><h1>" + "Hello Jersey" + "</body></h1>" + "</html> ";
  }

}
