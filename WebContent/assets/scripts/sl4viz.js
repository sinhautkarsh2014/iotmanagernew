var app = angular.module('myApp', ['ngRoute']);




app.controller('customersCtrl', function($scope, $http, $interval, $location) {
	
	
	
	$scope.markersArray = [];
	$scope.markerswhArray = [];
    $scope.polymasterArray=[];
    $scope.polyincrementalArray=[];
    
    var warehouse=[{"coords":{"lat":12.95453,"lng":77.60537},"whid":"a1"},{"coords":{"lat":12.96461,"lng":77.57561},"whid":"a2"},{"coords":{"lat":12.97769,"lng":77.62895},"whid":"a3"},{"coords":{"lat":12.99048,"lng":77.59955},"whid":"a4"},{"coords":{"lat":12.99048,"lng":77.59955},"whid":"720"}];

    
    
    
    // Create a scope counter_truckid=0 for polyline drawing (4 counters)
    $scope.plinecounter=new Array(4+1).join('0').split('').map(parseFloat);
    console.log($scope.plinecounter)
    
   
    
	// Calling the function every 6 min (as set in the interval)
	$scope.callAtInterval = function() {
	// Calling the REST API
	$http.get("http://localhost:1880/mongoapi")
    .then(function (response) {
    	//Clearing all the previously made markers
    	for (var i = 0; i < $scope.markersArray.length; i++ ) {
            $scope.markersArray[i].setMap(null);
            }
        $scope.markersArray.length = 0;
        
        for (var i = 0; i < $scope.markerswhArray.length; i++ ) {
            $scope.markerswhArray[i].setMap(null);
            }
        $scope.markerswhArray.length = 0;
    	
        //Getting the integrated mongodb collection 'rpimsg'
    	$scope.names = response.data;
    	
    	var datain=$scope.names[response.data.length-1].payload;
    	console.log(datain);
    	datain=JSON.parse(datain);
    	//Block for all truck's tracking
    	if($scope.viewState==="all"){

            for (var i=0; i<datain.length; i++){
                console.log(datain[i].coords);
                var latitude=parseFloat(datain[i].coords.lat);
                var longitude=parseFloat(datain[i].coords.lng);
                console.log(latitude);
                console.log(longitude);
                var latloncurrent=new google.maps.LatLng(latitude,longitude);
  		           

  		              var marker2 = new google.maps.Marker({
  		            	icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',   
  		            	  position: latloncurrent,
  			                    map: $scope.map,

  			                       title: "Eureka!"
  			                        });
  		                            $scope.markersArray.push(marker2);

         	                        console.log("Got marker at " + datain[i].coords.lat + ", " + datain[i].coords.lng, datain);
                                }
                              }

            else{
            var index = datain.findIndex(function(item, i){
            return item.truckid === $scope.viewState
               });
            console.log(index);
            $scope.masterplinecoords=[];
            //if counter==0   Logic for finding index for coords.lat==0 && truckid=xyz
            // draw the master polyline
            var directionsService = new google.maps.DirectionsService();
            
            
            if($scope.plinecounter[index]==0){
            	var ind=response.data.length-1;
            	startind=ind;
            	while(1){
            		if(JSON.parse($scope.names[ind].payload)[index].coords.lat==0){startind=ind; break;}
            		ind=ind-1;
            	}
            	//Create a coordinate array of everything from startindex+1 to response.data.length-1
            	for(i=startind+1; i<=response.data.length-1; i++){
            		$scope.masterplinecoords.push({lat:parseFloat(JSON.parse($scope.names[i].payload)[index].coords.lat),lng:parseFloat(JSON.parse($scope.names[i].payload)[index].coords.lng)})
            		
            		//Create a distance based logic for using directions api or not
                       		
            		
            	//	var polymaster = new google.maps.Polyline({
                //        path: $scope.masterplinecoords,
                //        geodesic: true,
                //        strokeColor: '#1e90ff',
                //        strokeOpacity: 0.1,
                //        strokeWeight: 4
                //         });
            	//	$scope.polymasterArray.push(polymaster);
                //    polymaster.setMap($scope.map);
            	
            	}
            	
            	
                console.log($scope.masterplinecoords[$scope.masterplinecoords.length-1]);
            	//Loop and Draw Path Route between the Points on MAP
                //for(i=0; i<=$scope.masterplinecoords.length-2; i++){
                
                //	setTimeout(function() {
                		
                        //var start = new google.maps.LatLng($scope.masterplinecoords[i].lat,$scope.masterplinecoords[i].lng);
                        //var end = new google.maps.LatLng($scope.masterplinecoords[i+1].lat,$scope.masterplinecoords[i+1].lng);
                        var start = new google.maps.LatLng($scope.masterplinecoords[0].lat,$scope.masterplinecoords[0].lng);
                        var end = new google.maps.LatLng($scope.masterplinecoords[$scope.masterplinecoords.length-1].lat,$scope.masterplinecoords[$scope.masterplinecoords.length-1].lng);
                        $scope.directionsDisplay.setMap(null);
                        var request = {
                          origin: start,
                          destination: end,
                          travelMode: google.maps.TravelMode.DRIVING
                        };
                        directionsService.route(request, function(response, status) {
                          if (status == google.maps.DirectionsStatus.OK) {
                        	$scope.directionsDisplay.set('preserveViewport',true);  
                            $scope.directionsDisplay.setDirections(response);
                            $scope.directionsDisplay.setMap($scope.map);
                            $scope.directionsDisplay.setOptions( { suppressMarkers: true });
                          } else {
                            alert("Directions111 Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                          }
                        });
                //		}, 200);	
               
                
                //}
        		

            	$scope.plinecounter[index]=1;
            }
        	//draw the incremental polyline
            $scope.incrementalplinecoords=[];
            $scope.incrementalplinecoords.push({lat:parseFloat(JSON.parse($scope.names[response.data.length-2].payload)[index].coords.lat),lng:parseFloat(JSON.parse($scope.names[response.data.length-2].payload)[index].coords.lng)})
        	$scope.incrementalplinecoords.push({lat:parseFloat(JSON.parse($scope.names[response.data.length-1].payload)[index].coords.lat),lng:parseFloat(JSON.parse($scope.names[response.data.length-1].payload)[index].coords.lng)})
            //Create a distance based logic for using directions api or not
        	//straight line drawing
        	//var polyincremental = new google.maps.Polyline({
            //            path: $scope.incrementalplinecoords,
            //            geodesic: true,
            //            strokeColor: '#1e90ff',
            //            strokeOpacity: 0.1,
            //            strokeWeight: 4
            //             });
            //		$scope.polyincrementalArray.push(polyincremental);
            //        polyincremental.setMap($scope.map);
            
            //directions api drawing
        	directionsDisplay2=new google.maps.DirectionsRenderer();
            var start = new google.maps.LatLng($scope.incrementalplinecoords[0].lat,$scope.incrementalplinecoords[0].lng);
                        var end = new google.maps.LatLng($scope.incrementalplinecoords[1].lat,$scope.incrementalplinecoords[1].lng);
                        
                        var request = {
                          origin: start,
                          destination: end,
                          travelMode: google.maps.TravelMode.DRIVING
                        };
                        directionsService.route(request, function(response, status) {
                          if (status == google.maps.DirectionsStatus.OK) {
                        	  directionsDisplay2.set('preserveViewport',true);  
                        	  directionsDisplay2.setDirections(response);
                        	  directionsDisplay2.setMap($scope.map);
                        	  directionsDisplay2.setOptions( { suppressMarkers: true });
                          } else {
                            alert("Directionsss Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                          }
                        });

                       $scope.directionsDisplay2Array.push(directionsDisplay2);
            
            
            
            
               //Warehouse Coords [to be fetched from TMS]
               //Add a post request, assign response coordinate to the warehouse coordinate
            var latitudewh=parseFloat(warehouse[index].coords.lat);
            var longitudewh=parseFloat(warehouse[index].coords.lng);
            console.log(longitudewh);
            var latloncurrentwh=new google.maps.LatLng(latitudewh,longitudewh);
            var markerwh = new google.maps.Marker({
            	icon: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png',
            	position: latloncurrentwh,
                   map: $scope.map,

                      title: "Warehouse ID"+": "+warehouse[index].whid
                    });
           $scope.markerswhArray.push(markerwh);
               //Truck's Coords
               var latitude=parseFloat(datain[index].coords.lat);
               var longitude=parseFloat(datain[index].coords.lng);
               console.log(latitude);
               console.log(longitude);
               var latloncurrent=new google.maps.LatLng(latitude,longitude);
                 

                   var marker2 = new google.maps.Marker({
                	   icon: 'http://maps.google.com/mapfiles/ms/icons/blue-dot.png', 
                	   position: latloncurrent,
                           map: $scope.map,

                              title: "Safety_State"+"  "+datain[index].safety_stat
                            });
                   $scope.markersArray.push(marker2);
                   
                   
                   //TODO Add Loading, Check-in, Unloading, Checkout processes by listening to TMS events 
                   //TODO Add warehouse geofence, if entered, http post request to TMS
                   
                   
                   //ETA Calculation
                   var service = new google.maps.DistanceMatrixService();
                   service.getDistanceMatrix(
                     {
                       origins: [latloncurrent],
                       destinations: [latloncurrentwh],
                       travelMode: 'DRIVING',
                       //transitOptions: TransitOptions,
                       //drivingOptions: DrivingOptions,
                       //unitSystem: UnitSystem,
                       //avoidHighways: Boolean,
                       //avoidTolls: Boolean,
                     }, callback);

                   function callback(response, status) {
                     console.log(response.rows[0].elements[0].duration.text);
                     $scope.eta=response.rows[0].elements[0].duration.text;
                     if(latitude==latitudewh&&longitude==longitudewh){$scope.eta="Delivered";}
                   }
                   
                  
                  
                  console.log("Got marker at " + datain[index].coords.lat + ", " + datain[index].coords.lng, datain+$scope.markersArray.length); 

                          }
        
        
    
    }).catch(function (data) {
        // Handle error here
    	$scope.names = [{"Name":"Vineet", "Country" :"India" }];
    });
	}
	
	
	//Maintaining a counter to avoid error when no. of calls are null
	$scope.promisecounter=0;
	//Starting the tracking when form input is taken
	$scope.track = function() {
		console.log("let's start!")
		// Create a scope counter_truckid=0 for polyline drawing (4 counters)
		//Cancelling the all truck tracking promise
		$interval.cancel(promise1);
		//Cancelling the previous truck tracking promise
		if($scope.promisecounter>0){$interval.cancel(promise2);}
		
		
		
		
    	
		//Clearing the masterpolyline corresponding to the previous truck being tracked
		//1
		for (var i = 0; i < $scope.polymasterArray.length; i++ ) {
            $scope.polymasterArray[i].setMap(null);
            }
        $scope.polymasterArray.length = 0;
        
        //2
        $scope.directionsDisplay.setMap(null);
        
        
        
        
      //Clearing the incrementalpolylines corresponding to the previous truck being tracked
	  //1
        for (var i = 0; i < $scope.polyincrementalArray.length; i++ ) {
            $scope.polyincrementalArray[i].setMap(null);
            }
        $scope.polyincrementalArray.length = 0;
      //2
        for (var i = 0; i < $scope.directionsDisplay2Array.length; i++ ) {
        	$scope.directionsDisplay2Array[i].setMap(null);
            }
        
        

		$scope.viewState=angular.copy($scope.trackabletruckid);
		
		$scope.promisecounter+=1;
		//Clearing the pline counter
		$scope.plinecounter=new Array(4+1).join('0').split('').map(parseFloat);
	    console.log($scope.plinecounter)
		//Setting an interval to call the function
	    promise2=$interval( function(){ $scope.callAtInterval(); }, 1200);
    };
    
    
    
  
    
    
    
    //Starting the tracking for all trucks
	$scope.trackinit = function() {
		console.log("let's start!")
		var myLatlng = new google.maps.LatLng(12.97578,77.59976);
	var mapOptions = {
	    zoom: 12,
	    center: myLatlng
	  };
		$scope.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		//Directions Renderer
		
		$scope.directionsDisplay = new google.maps.DirectionsRenderer();
    	$scope.directionsDisplay2Array = [];
    	$scope.directionsDisplay.setMap(null);
		
		//Setting an interval to call the function
	    promise1=$interval( function(){ $scope.callAtInterval(); }, 1200);
    };
    //Tracking the load by taking loadid from URL
    $scope.search = $location.search();
    console.log($scope.search.loadid);
    if($scope.search.loadid){$scope.viewState=$scope.search.loadid;}
    else{$scope.viewState="all";}
	$scope.trackinit();
});