var app = angular.module('myApp', []);
app.controller('customersCtrl', function($scope, $http, $interval) {
	var myLatlng = new google.maps.LatLng(54.9619349,-1.6003813);
	var mapOptions = {
	    zoom: 10,
	    center: myLatlng
	  };
	$scope.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	$scope.markersArray = [];
    $scope.PathCoordinates=[];
    $scope.pathsArray=[];
	// Calling the function every 1s (as set in the interval)
	$scope.callAtInterval = function() {
	// Calling the REST API
	$http.get("http://localhost:8080/IOTManager/app/rest/od")
    .then(function (response) {
    	//Clearing all the previously made markers
    	for (var i = 0; i < $scope.markersArray.length; i++ ) {
            $scope.markersArray[i].setMap(null);
            }
        $scope.markersArray.length = 0;
    	
        
    	$scope.names = response.data;
    	var datain=$scope.names;
    	//Block for all truck's tracking
    	if($scope.viewState==="all"){

            for (var i=0; i<4; i++){

                var latitude=parseFloat(datain[i].coords.lat);
                var longitude=parseFloat(datain[i].coords.lng);
                console.log(latitude);
                console.log(longitude);
                var latloncurrent=new google.maps.LatLng(latitude,longitude);
  		            console.log(myLatlng);

  		              var marker2 = new google.maps.Marker({
  			                 position: latloncurrent,
  			                    map: $scope.map,

  			                       title: "Eureka!"
  			                        });
  		                            $scope.markersArray.push(marker2);

         	                        console.log("Got marker at " + datain[i].coords.lat + ", " + datain[i].coords.lng, datain);
                                }
                              }

            else{
            var index = datain.findIndex(function(item, i){
            return item.truckid === $scope.viewState
               });
            console.log(index);
               var latitude=parseFloat(datain[index].coords.lat);
               var longitude=parseFloat(datain[index].coords.lng);
               console.log(latitude);
               console.log(longitude);
               var latloncurrent=new google.maps.LatLng(latitude,longitude);
                 console.log(myLatlng);

                   var marker2 = new google.maps.Marker({
                        position: latloncurrent,
                           map: $scope.map,

                              title: "Eureka!"
                            });
                   $scope.markersArray.push(marker2);
                   for (var i=0; i<$scope.markersArray.length; i++){$scope.PathCoordinates.push({lat:$scope.markersArray[i].getPosition().lat(),lng:$scope.markersArray[i].getPosition().lng()})}

                   var path = new google.maps.Polyline({
                       path: $scope.PathCoordinates,
                       geodesic: true,
                       strokeColor: '#0064fa',
                       strokeOpacity: 1.0,
                       strokeWeight: 2
                        });
                  $scope.pathsArray.push(path);
                  path.setMap($scope.map);
                  console.log("Got marker at " + datain[index].coords.lat + ", " + datain[index].coords.lng, datain+$scope.markersArray.length); 

                          }
        
        
    
    }).catch(function (data) {
        // Handle error here
    	$scope.names = [{"Name":"Vineet", "Country" :"India" }];
    });
	}
	
	
	//Maintaining a counter to avoid error when no. of calls are null
	$scope.promisecounter=0;
	//Starting the tracking when form input is taken
	$scope.track = function() {
		console.log("let's start!")
		//Cancelling the all truck tracking promise
		$interval.cancel(promise1);
		//Cancelling the previous truck tracking promise
		if($scope.promisecounter>0){$interval.cancel(promise2);}
		
		//Clearing the polylines corresponding to the previous truck being tracked
		for (var i = 0; i < $scope.pathsArray.length; i++ ) {
            $scope.pathsArray[i].setMap(null);
            }
        $scope.pathsArray.length = 0;
        
        //Clearing the array of coordinates which belonged to the previous truck
        $scope.PathCoordinates.length=0;
        

		$scope.viewState=angular.copy($scope.trackabletruckid);
		$scope.promisecounter+=1;
		//Setting an interval to call the function
	    promise2=$interval( function(){ $scope.callAtInterval(); }, 1000);
    };
    
    
    
  //TODO Add a function initiated by a button for saved search feature 
  //TODO Export the polyline object to a DB [Saved Search]
  //TODO Export the array of coordinates to a DB [Save Search]  
    
    
    
    //Starting the tracking for all trucks
	$scope.trackinit = function() {
		console.log("let's start!")
		//Setting an interval to call the function
		
	    promise1=$interval( function(){ $scope.callAtInterval(); }, 1000);
    };
    //Making the first function call for all trucks' tracking with a default "viewState"
    $scope.viewState="all";
	$scope.trackinit();
});